<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace HTTP;

use InvalidArgumentException;
use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\StreamInterface;

abstract class AbstractMessage implements MessageInterface
{
    protected const TOKEN_PATTERN = '/^[!#$%&\'*+.^_`|~0-9A-Za-z-]+$/';

    private array $headers = [];
    private array $headerNames = [];

    private string $protocolVersion = '1.1';
    private ?StreamInterface $body = null;

    public function getProtocolVersion()
    {
        return $this->protocolVersion;
    }

    protected function setProtocolVersion($version)
    {
        if (!is_string($version)) {
            throw new InvalidArgumentException('Version must be a string');
        }

        $this->protocolVersion = $version;
    }

    public function withProtocolVersion($version)
    {
        $new = clone $this;
        $new->setProtocolVersion($version);

        return $new;
    }

    public function getHeaders()
    {
        return $this->headers;
    }

    public function hasHeader($name)
    {
        return in_array($name, $this->headerNames);
    }

    public function getHeader($name)
    {
        if ($this->hasHeader($name)) {
            $normalizedName = $this->normalizeHeaderName($name);
            $headerName = $this->headerNames[$normalizedName] ?? $name;

            return $this->headers[$headerName];
        } else {
            return [];
        }
    }

    protected function setHeader($name, $value, bool $first = false)
    {
        $this->validHeaderName($name);
        $values = $this->validAndNormalizeHeaderValue($value);

        $normalizedName = strtolower($name);
        $headerName = $this->headerNames[$normalizedName] ?? $name;

        if ($first) {
            // Place it at the beginning of the headers array
            $this->headers = [$headerName => $values] + $this->headers;
        } else {
            $this->headers[$headerName] = $values;
        }

        $this->headerNames[$headerName] = $headerName;
    }

    public function getHeaderLine($name)
    {
        return implode(',', $this->getHeader($name));
    }

    public function withHeader($name, $value)
    {
        $new = clone $this;
        $new->setHeader($name, $value);

        return $new;
    }

    public function withAddedHeader($name, $value)
    {
        if ($this->hasHeader($name)) {
            $values = $this->validAndNormalizeHeaderValue($value);

            $normalizedName = $this->normalizeHeaderName($name);
            $headerName = $this->headerNames[$normalizedName] ?? $name;

            $new = clone $this;
            $new->headers[$headerName] = array_merge($new->headers[$headerName], $values);

            return $new;
        } else {
            return $this->withHeader($name, $value);
        }
    }

    public function withoutHeader($name)
    {
        if ($this->hasHeader($name)) {
            $new = clone $this;

            if ($this->hasHeader($name)) {
                $normalizedName = $this->normalizeHeaderName($name);
                $headerName = $this->headerNames[$normalizedName] ?? $name;

                unset($new->headers[$headerName]);
                unset($new->headerNames[$normalizedName]);
            }

            return $new;
        } else {
            return $this;
        }
    }

    public function getBody()
    {
        return $this->body;
    }

    public function withBody(StreamInterface $body)
    {
        $new = clone $this;
        $new->body = $body;

        return $new;
    }

    private function normalizeHeaderName($name)
    {
        if (!is_string($name)) {
            throw new InvalidArgumentException('Header name must be a string');
        }

        return strtolower($name);
    }

    private function validHeaderName($name)
    {
        if (!is_string($name)) {
            throw new InvalidArgumentException('Header name must be a string');
        }

        if (!preg_match(self::TOKEN_PATTERN, $name)) {
            throw new InvalidArgumentException('Header name must be compatible with RFC 7230');
        }
    }

    private function validAndNormalizeHeaderValue($value)
    {
        if (is_string($value)) {
            $value = [$value];
        }

        if (is_array($value)) {
            if (empty($value)) {
                throw new InvalidArgumentException('Header value must be a non empty array');
            }

            $normalizedValues = [];

            foreach ($value as $v) {
                if (!is_string($v) || !preg_match('/^[ \t\x21-\x7E\x80-\xFF]*$/', $v)) {
                    throw new InvalidArgumentException('Invalid header value. It must be compatible with RFC 7230');
                } else {
                    $normalizedValues[] = trim($v);
                }
            }

            return $normalizedValues;
        } else {
            throw new InvalidArgumentException('Header value must be a string or an array');
        }
    }
}
