<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace HTTP\Stream;

use InvalidArgumentException;
use Psr\Http\Message\StreamInterface;
use RuntimeException;

use function fstat;

final class Stream implements StreamInterface
{
    private const READABLE_WRITABLE_HASH = [
        'readable' => [
            'r',
            'w+',
            'r+',
            'x+',
            'c+',
            'rb',
            'w+b',
            'r+b',
            'x+b',
            'c+b',
            'rt',
            'w+t',
            'r+t',
            'x+t',
            'c+t',
            'a+',
        ],
        'writable' => [
            'w',
            'w+',
            'rw',
            'r+',
            'x+',
            'c+',
            'wb',
            'w+b',
            'r+b',
            'x+b',
            'c+b',
            'w+t',
            'r+t',
            'x+t',
            'c+t',
            'a',
            'a+',
        ],
    ];

    /**
     * @var resource
     */
    private $stream;

    private bool $seekable;
    private bool $writable;
    private bool $readable;

    private int $size;

    /**
     * Stream constructor.
     * @param resource $resource
     */
    public function __construct($resource)
    {
        if (!is_resource($resource)) {
            throw new InvalidArgumentException('Resource must be a resource');
        }

        $this->stream = $resource;

        $metadata = $this->getMetadata();
        $this->seekable = (bool)$metadata['seekable'];

        $mode = $metadata['mode'];
        $this->readable = isset(self::READABLE_WRITABLE_HASH['readable'][$mode]);
        $this->writable = isset(self::READABLE_WRITABLE_HASH['writable'][$mode]);

        $this->setSize();
    }

    public function __destruct()
    {
        $this->close();
    }

    /**
     * @inheritDoc
     */
    public function __toString()
    {
        $this->rewind();

        return $this->getContents();
    }

    /**
     * @inheritDoc
     */
    public function close()
    {
        fclose($this->stream);

        $this->detach();
    }

    /**
     * @inheritDoc
     */
    public function detach()
    {
        unset($this->stream);
        unset($this->stream);
        unset($this->seekable);
        unset($this->writable);
        unset($this->size);
    }

    /**
     * @inheritDoc
     */
    public function getSize()
    {
        if (isset($this->size) || $this->setSize()) {
            return $this->size;
        } else {
            return null;
        }
    }

    private function setSize()
    {
        $stats = fstat($this->stream);

        if (isset($stats['size'])) {
            $this->size = $stats['size'];

            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritDoc
     */
    public function tell()
    {
        $tell = ftell($this->stream);

        if ($tell === false) {
            throw new RuntimeException('Exception while get current position');
        }

        return $tell;
    }

    /**
     * @inheritDoc
     */
    public function eof()
    {
        return $this->getMetadata('eof') || feof($this->stream);
    }

    /**
     * @inheritDoc
     */
    public function isSeekable()
    {
        return $this->seekable;
    }

    /**
     * @inheritDoc
     */
    public function seek($offset, $whence = SEEK_SET)
    {
        if (!$this->isSeekable()) {
            throw new RuntimeException('The stream is not seekable');
        }

        if (fseek($this->stream, $offset, $whence) === 1) {
            throw new RuntimeException('Exception while seeking stream failed');
        }
    }

    /**
     * @inheritDoc
     */
    public function rewind()
    {
        $this->seek(0);
    }

    /**
     * @inheritDoc
     */
    public function isWritable()
    {
        return $this->writable;
    }

    /**
     * @inheritDoc
     */
    public function write($string)
    {
        if (!$this->isWritable()) {
            throw new RuntimeException('The stream is not writable');
        }

        fwrite($this->stream, $string);
    }

    /**
     * @inheritDoc
     */
    public function isReadable()
    {
        return $this->readable;
    }

    /**
     * @inheritDoc
     */
    public function read($length)
    {
        if (!$this->isReadable()) {
            throw new RuntimeException('The stream is not readable');
        }

        return fread($this->stream, $length);
    }

    /**
     * @inheritDoc
     */
    public function getContents()
    {
        $contents = stream_get_contents($this->stream);

        if ($contents === false) {
            throw new RuntimeException('Cannot get the stream contents');
        }

        return $contents;
    }

    /**
     * @inheritDoc
     */
    public function getMetadata($key = null)
    {
        $metadata = stream_get_meta_data($this->stream);

        if ($key == null) {
            return $metadata;
        } else {
            return $metadata[$key] ?? null;
        }
    }
}
