<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace HTTP\Request\Server;

use HTTP\Files\UploadedFileFactory;
use HTTP\Stream\StreamFactory;
use HTTP\URI\Uri;
use HTTP\URI\UriFactory;
use Psr\Http\Message\ServerRequestFactoryInterface;
use Psr\Http\Message\ServerRequestInterface;

class ServerRequestFactory implements ServerRequestFactoryInterface
{
    public static function createServerRequestFromGlobals()
    {
        $method = $_SERVER['REQUEST_METHOD'];

        $uri = new Uri('');
        $uri = $uri->withScheme(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http');
        $uri = $uri->withHost($_SERVER['SERVER_NAME']);

        if (isset($_SERVER['SERVER_PORT'])) {
            $uri = $uri->withPort((int) $_SERVER['SERVER_PORT']);
        }

        if (isset($_SERVER['PATH_INFO'])) {
            $uri = $uri->withPath($_SERVER['PATH_INFO']);
        }

        if (isset($_SERVER['QUERY_STRING'])) {
            $uri = $uri->withQuery($_SERVER['QUERY_STRING']);
        }

        $instance = new ServerRequest($method, $uri, $_SERVER);

        $instance->withCookieParams($_COOKIE);
        $instance->withQueryParams($_GET);

        $uploadedFiles = [];

        foreach ($_FILES as $FILE) {
            $stream = (new StreamFactory())->createStreamFromFile($FILE['tmp_name']);
            $size = $FILE['size'];
            $error = $FILE['error'];
            $name = $FILE['name'] ?? null;
            $type = $FILE['type'] ?? null;

            $file = (new UploadedFileFactory())->createUploadedFile($stream, $size, $error, $name, $type);

            $uploadedFiles[] = $file;
        }

        $instance->withUploadedFiles($uploadedFiles);

        return $instance;
    }

    /**
     * @inheritDoc
     */
    public function createServerRequest(string $method, $uri, array $serverParams = []): ServerRequestInterface
    {
        if (is_string($uri)) {
            $uri = (new UriFactory())->createUri($uri);
        }

        return new ServerRequest($method, $uri, $serverParams);
    }
}
