<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace HTTP\Request;

use HTTP\AbstractMessage;
use InvalidArgumentException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\UriInterface;

class Request extends AbstractMessage implements RequestInterface
{
    private string $method;
    private UriInterface $uri;
    private ?string $requestTarget;

    /**
     * Request constructor.
     * @param string $method
     * @param UriInterface $uri
     * @param string|null $requestTarget
     */
    public function __construct(string $method, UriInterface $uri, ?string $requestTarget = null)
    {
        $this->setMethod($method);
        $this->setUri($uri);
        $this->setRequestTarget($requestTarget);
    }

    public function getRequestTarget()
    {
        if (isset($this->requestTarget)) {
            return $this->requestTarget;
        } elseif (isset($this->uri)) {
            $requestTarget = empty($this->uri->getPath()) ? '/' : $this->uri->getPath(); // Add path

            $query = $this->uri->getQuery();
            if (!empty($query)) {
                $requestTarget .= '?' . $query; // Add query
            }

            return $requestTarget;
        } else {
            return '/';
        }
    }

    private function setRequestTarget($requestTarget)
    {
        if (!is_string($requestTarget) && !is_null($requestTarget)) {
            throw new InvalidArgumentException('Request target must be a string');
        }

        $this->requestTarget = $requestTarget;
    }

    public function withRequestTarget($requestTarget)
    {
        $new = clone $this;
        $new->setRequestTarget($requestTarget);

        return $new;
    }

    public function getMethod()
    {
        return $this->method;
    }

    private function setMethod($method)
    {
        if (!is_string($method)) {
            throw new InvalidArgumentException('Method must be a string');
        }

        if (preg_match(parent::TOKEN_PATTERN, $method) !== 1) {
            throw new InvalidArgumentException('Method must compatible with RFC 7230');
        }

        $this->method = $method;
    }

    public function withMethod($method)
    {
        $new = clone $this;
        $new->setMethod($method);

        return $new;
    }

    public function getUri()
    {
        return $this->uri;
    }

    private function setUri(UriInterface $uri, $preserveHost = false)
    {
        $this->uri = $uri;

        if (!$preserveHost) {
            $this->updateHost();
        }
    }

    public function withUri(UriInterface $uri, $preserveHost = false)
    {
        if ($uri === $this->uri) {
            return $this;
        } else {
            $new = clone $this;
            $new->setUri($uri, $preserveHost);

            return $new;
        }
    }

    private function updateHost(): void
    {
        if (!$this->hasHeader('Host') && !empty($this->uri->getHost())) {
            // Update host if uri has one
            $host = $this->uri->getHost();

            if (!is_null($this->uri->getPort())) {
                $host .= ':' . $this->uri->getPort(); // Add port if uri has one
            }

            $this->setHeader('Host', $host, true);
        }
    }
}
