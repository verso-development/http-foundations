<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace HTTP\URI;

use InvalidArgumentException;
use Psr\Http\Message\UriInterface;
use RuntimeException;

final class Uri implements UriInterface
{
    private const SUB_DELIMITERS = '!$&\'\()\*\+,;=';
    private const UNRESERVED = 'a-zA-Z0-9\-._~';

    private const P_CHAR = self::UNRESERVED . self::SUB_DELIMITERS . ':@';
    private const PCT_ENCODED = '%(?![a-zA-Z0-9]{2})';

    private const SCHEME_PATTERN = '/^[a-zA-Z][a-zA-Z0-9+\-.]*$/';
    private const PATH_PATTERN = '/(?:[^' . self::P_CHAR . '%\/]|' . self::PCT_ENCODED . ')/';
    private const QUERY_FRAGMENT_PATTERN = '/(?:[^' . self::P_CHAR . '%\/\?]|' . self::PCT_ENCODED . ')/';

    private const PORT_LOWER_LIMIT = 0;
    private const PORT_UPPER_LIMIT = 65535;

    private const SCHEME_STANDARD_PORTS = [
        'http' => 80,
        'https' => 443
    ];

    private const URL_SCHEME = 'scheme';
    private const URL_USER = 'user';
    private const URL_PASSWORD = 'pass';
    private const URL_HOST = 'host';
    private const URL_PORT = 'port';
    private const URL_PATH = 'path';
    private const URL_QUERY = 'query';
    private const URL_FRAGMENT = 'fragment';

    private string $scheme;
    private string $user;
    private ?string $password;
    private string $host;
    private ?int $port;
    private string $path;
    private string $query;
    private string $fragment;

    /**
     * Uri constructor.
     * @param string $uri
     */
    public function __construct(string $uri)
    {
        $components = parse_url($uri);

        if ($components === false) {
            throw new InvalidArgumentException('Unable to parse uri');
        } else {
            $this->setScheme($components[self::URL_SCHEME] ?? '');
            $this->setUser($components[self::URL_USER] ?? '');
            $this->setPassword($components[self::URL_PASSWORD] ?? null);
            $this->setHost($components[self::URL_HOST] ?? '');
            $this->setPort($components[self::URL_PORT] ?? null);
            $this->setPath($components[self::URL_PATH] ?? '');
            $this->setQuery($components[self::URL_QUERY] ?? '');
            $this->setFragment($components[self::URL_FRAGMENT] ?? '');
        }
    }

    private function setScheme($scheme)
    {
        if (!is_string($scheme)) {
            throw new InvalidArgumentException('Scheme must be a string');
        }

        if (empty($scheme)) {
            $this->scheme = $scheme;
        } elseif (preg_match(self::SCHEME_PATTERN, $scheme)) {
            $normalizedScheme = $this->normalize($scheme);

            $this->scheme = $normalizedScheme;
        } else {
            throw new InvalidArgumentException('Scheme must be compatible with RFC 7230');
        }
    }

    private function normalize($uri)
    {
        if (!is_string($uri)) {
            throw new InvalidArgumentException('Uri must be a string');
        }

        return strtolower($uri);
    }

    private function setUser($user)
    {
        if (!is_string($user)) {
            throw new InvalidArgumentException('User must be a string');
        }

        $this->user = $user;
    }

    private function setPassword($password)
    {
        if (!is_string($password) && !is_null($password)) {
            throw new InvalidArgumentException('Password must be a string or null');
        }

        $this->password = $password;
    }

    private function setHost($host)
    {
        if (!is_string($host)) {
            throw new InvalidArgumentException('Host must be a string');
        }

        if (empty($host)) {
            $this->host = $host;
        } else {
            $normalizedHost = $this->normalize($host);
            $this->host = $normalizedHost;
        }
    }

    private function setPort($port)
    {
        if (!is_int($port) && !is_null($port)) {
            throw new InvalidArgumentException('Port must be an integer or null');
        }

        if (is_int($port)) {
            if ($port < self::PORT_LOWER_LIMIT || $port > self::PORT_UPPER_LIMIT) {
                throw new InvalidArgumentException(
                    sprintf(
                        'Port is outside bounds. It must be between %d and %d',
                        self::PORT_LOWER_LIMIT,
                        self::PORT_UPPER_LIMIT
                    )
                );
            }
        }

        $this->port = $port;
    }

    private function setPath($path)
    {
        if (!is_string($path)) {
            throw new InvalidArgumentException('Path must be a string');
        }

        $this->path = $this->encodeUrl($path, self::PATH_PATTERN);
    }

    private function encodeUrl($uri, $regex)
    {
        if (!is_string($uri)) {
            throw new InvalidArgumentException('Uri must be a string');
        }

        $encodedUri = preg_replace_callback(
            $regex,
            function ($match) {
                return rawurlencode($match[0]);
            },
            $uri
        );

        if (is_null($encodedUri)) {
            throw new RuntimeException('An error occurred while encoding the uri');
        }

        $normalizedCapitalizedUri = preg_replace_callback(
            '/%[a-zA-Z0-9]{2,}/',
            function ($match) {
                return strtoupper($match[0]);
            },
            $encodedUri
        ); // Normalize encoded uri by capitalizing encoded sequences

        if (is_null($normalizedCapitalizedUri)) {
            throw new RuntimeException('An error occurred while encoding the uri');
        }

        return $normalizedCapitalizedUri;
    }

    private function setQuery($query)
    {
        if (!is_string($query)) {
            throw new InvalidArgumentException('Query must be a string');
        }

        if (empty($query)) {
            $this->query = $query;
        } else {
            $this->query = $this->encodeUrl($query, self::QUERY_FRAGMENT_PATTERN);
        }
    }

    private function setFragment($fragment)
    {
        if (!is_string($fragment)) {
            throw new InvalidArgumentException('Fragment must be a string');
        }

        if (empty($fragment)) {
            $this->fragment = $fragment;
        } else {
            $this->fragment = $this->encodeUrl($fragment, self::QUERY_FRAGMENT_PATTERN);
        }
    }

    /**
     * @inheritDoc
     */
    public function withScheme($scheme)
    {
        $new = clone $this;
        $new->setScheme($scheme);

        return $new;
    }

    /**
     * @inheritDoc
     */
    public function withUserInfo($user, $password = null)
    {
        $new = clone $this;
        $new->setUser($user);
        $new->setPassword($password);

        return $new;
    }

    /**
     * @inheritDoc
     */
    public function withHost($host)
    {
        $new = clone $this;
        $new->setHost($host);

        return $new;
    }

    /**
     * @inheritDoc
     */
    public function withPort($port)
    {
        $new = clone $this;
        $new->setPort($port);

        return $new;
    }

    /**
     * @inheritDoc
     */
    public function withPath($path)
    {
        $new = clone $this;
        $new->setPath($path);

        return $new;
    }

    /**
     * @inheritDoc
     */
    public function withQuery($query)
    {
        $new = clone $this;
        $new->setQuery($query);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function withFragment($fragment)
    {
        $new = clone $this;
        $new->setFragment($fragment);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function __toString()
    {
        $uri = '';

        $scheme = $this->getScheme();

        if (!empty($scheme)) {
            $uri .= $scheme . ':';
        }

        $authority = $this->getAuthority();

        if (!empty($authority)) {
            $uri .= '//' . $authority;
        }

        $path = $this->getPath();

        if (!empty($path)) {
            if ($path[0] === '/') {
                // Absolute path
                if (empty($authority)) {
                    $path = ltrim($path, '/'); // Reduce starting slashes to one
                }
            } elseif (!empty($authority)) {
                $path = '/' . $path; // Prefix the path by '/'
            }
        }

        $uri .= $path;

        $query = $this->getQuery();

        if (!empty($query)) {
            $uri .= '?' . $query;
        }

        $fragment = $this->getFragment();

        if (!empty($fragment)) {
            $uri .= '#' . $fragment;
        }

        return $uri;
    }

    /**
     * @inheritDoc
     */
    public function getScheme()
    {
        return $this->scheme;
    }

    /**
     * @inheritDoc
     */
    public function getAuthority()
    {
        $host = $this->getHost();

        if (empty($host)) {
            return '';
        } else {
            $authority = '';

            $userInfo = $this->getUserInfo();
            if (!empty($userInfo)) {
                $authority .= $userInfo . '@';
            }

            $authority .= $host;

            $port = $this->getPort();
            if (!is_null($port)) {
                $authority .= ':' . $port;
            }

            return $authority;
        }
    }

    /**
     * @inheritDoc
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @inheritDoc
     */
    public function getUserInfo()
    {
        $user = $this->getUser();

        if (empty($user)) {
            return '';
        } else {
            $userInfo = $user;

            $pass = $this->getPassword();

            if (!is_null($pass)) {
                $userInfo .= ':' . $pass;
            }

            return $userInfo;
        }
    }

    private function getUser()
    {
        return $this->user;
    }

    private function getPassword()
    {
        return $this->password;
    }

    /**
     * @inheritDoc
     */
    public function getPort()
    {
        if (is_null($this->port)) {
            $standardPort = self::SCHEME_STANDARD_PORTS[$this->getScheme()];

            if ($this->port === $standardPort) {
                // Standard port
                return null;
            } else {
                // Non-standard port
                return $this->port;
            }
        } else {
            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @inheritDoc
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @inheritDoc
     */
    public function getFragment()
    {
        return $this->fragment;
    }
}
