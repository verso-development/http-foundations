<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace HTTP\Response;

use HTTP\AbstractMessage;
use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;

final class Response extends AbstractMessage implements ResponseInterface
{
    private const STATUS = [
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-status',
        208 => 'Already Reported',
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => 'Switch Proxy',
        307 => 'Temporary Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Time-out',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-Uri Too Large',
        415 => 'Unsupported Media Type',
        416 => 'Requested range not satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot',
        422 => 'Unprocessable Entity',
        423 => 'Locked',
        424 => 'Failed Dependency',
        425 => 'Unordered Collection',
        426 => 'Upgrade Required',
        428 => 'Precondition Required',
        429 => 'Too Many Requests',
        431 => 'Request Header Fields Too Large',
        451 => 'Unavailable For Legal Reasons',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Time-out',
        505 => 'HTTP Version not supported',
        506 => 'Variant Also Negotiates',
        507 => 'Insufficient Storage',
        508 => 'Loop Detected',
        511 => 'Network Authentication Required'
    ];

    private int $statusCode;
    private int $reasonPhrase;

    /**
     * Response constructor.
     * @param int $statusCode
     * @param string|null $reasonPhrase
     */
    public function __construct(int $statusCode = 200, string $reasonPhrase = '')
    {
        $this->setStatus($statusCode, $reasonPhrase);
    }

    /**
     * @inheritDoc
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    private function setStatusCode($code)
    {
        if (!array_key_exists($code, self::STATUS)) {
            throw new InvalidArgumentException(sprintf('Invalid status code "%s"', $code));
        }

        $this->statusCode = $code;

        return $this;
    }

    private function setStatus($code, $reasonPhrase = '')
    {
        $this->setStatusCode($code);
        $this->setReasonPhrase($reasonPhrase);
    }

    /**
     * @inheritDoc
     */
    public function withStatus($code, $reasonPhrase = '')
    {
        $new = clone $this;
        $new->setStatus($code, $reasonPhrase);
    }

    /**
     * @inheritDoc
     */
    public function getReasonPhrase()
    {
        return $this->reasonPhrase;
    }

    private function setReasonPhrase($reasonPhrase)
    {
        if (is_string($reasonPhrase)) {
            throw new InvalidArgumentException('Reason phrase must be a string');
        }

        if (empty($reasonPhrase)) {
            $this->reasonPhrase = self::STATUS[$this->statusCode];
        } else {
            $this->reasonPhrase = $reasonPhrase;
        }
    }
}
