<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace HTTP\Files;

use HTTP\Stream\StreamFactory;
use InvalidArgumentException;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UploadedFileInterface;
use RuntimeException;

final class UploadedFile implements UploadedFileInterface
{
    private const UPLOAD_ERRORS = [
        UPLOAD_ERR_OK,
        UPLOAD_ERR_INI_SIZE,
        UPLOAD_ERR_FORM_SIZE,
        UPLOAD_ERR_PARTIAL,
        UPLOAD_ERR_NO_FILE,
        UPLOAD_ERR_NO_TMP_DIR,
        UPLOAD_ERR_CANT_WRITE,
        UPLOAD_ERR_EXTENSION
    ];

    private StreamInterface $stream;
    private ?int $size;
    private int $error;
    private ?string $clientFileName;
    private ?string $clientMediaType;
    private bool $moved;

    /**
     * UploadedFile constructor.
     * @param StreamInterface $stream
     * @param int|null $size
     * @param int $error
     * @param string|null $clientFilename
     * @param string|null $clientMediaType
     */
    public function __construct(
        StreamInterface $stream,
        ?int $size = null,
        int $error = UPLOAD_ERR_OK,
        ?string $clientFilename = null,
        ?string $clientMediaType = null
    ) {
        $this->setStream($stream);
        $this->setSize($size);
        $this->setError($error);
        $this->setClientFilename($clientFilename);
        $this->setClientMediaType($clientMediaType);

        $this->moved = false;
    }

    private function validate()
    {
        if ($this->getError() !== UPLOAD_ERR_OK) {
            throw new RuntimeException('An upload error has occurred. Cannot perform treatment');
        }

        if ($this->moved) {
            throw new RuntimeException('The file has been moved');
        }
    }

    /**
     * @inheritDoc
     */
    public function getStream()
    {
        $this->validate();

        if (!isset($this->stream)) {
            throw new RuntimeException('Stream is not available');
        }

        return $this->stream;
    }

    private function setStream($stream)
    {
        if (!($stream instanceof StreamInterface)) {
            throw new InvalidArgumentException('Stream must be an instance of StreamInterface');
        }

        $this->stream = $stream;
    }

    /**
     * @inheritDoc
     */
    public function moveTo($targetPath)
    {
        $this->validate();

        if (!is_string($targetPath) || empty($targetPath)) {
            throw new InvalidArgumentException('Target path must be a non empty string');
        }

        $stream = $this->getStream();
        if ($stream->isSeekable()) {
            $stream->rewind();
        }

        $contents = $stream->getContents();

        $destination = (new StreamFactory())->createStreamFromFile($targetPath, 'w');
        $destination->write($contents);

        $this->moved = true;
    }

    /**
     * @inheritDoc
     */
    public function getSize()
    {
        return $this->size;
    }

    private function setSize($size)
    {
        if (is_null($size)) {
            $size = $this->getStream()->getSize();
        } elseif (!is_int($size)) {
            throw new InvalidArgumentException('Size must be null or an integer');
        }

        $this->size = $size;
    }

    /**
     * @inheritDoc
     */
    public function getError()
    {
        return $this->error;
    }

    private function setError(int $error)
    {
        if (is_int($error)) {
            throw new InvalidArgumentException('Error must be an integer');
        }

        if (!in_array($error, self::UPLOAD_ERRORS)) {
            throw new InvalidArgumentException(
                <<<'TAG'
                    Invalid error code. 
                    See https://www.php.net/manual/fr/features.file-upload.errors.php for a list of valid codes
                    TAG
            );
        }

        $this->error = $error;
    }

    /**
     * @inheritDoc
     */
    public function getClientFilename()
    {
        return $this->clientFileName;
    }

    private function setClientFilename($clientFilename)
    {
        if (!is_string($clientFilename) && !is_null($clientFilename)) {
            throw new InvalidArgumentException('Client file name must be a string or null');
        }

        $this->clientFileName = $clientFilename;
    }

    /**
     * @inheritDoc
     */
    public function getClientMediaType()
    {
        return $this->clientMediaType;
    }

    private function setClientMediaType($clientMediaType)
    {
        if (!is_string($clientMediaType) && !is_null($clientMediaType)) {
            throw new InvalidArgumentException('Client media type must be a string or null');
        }

        $this->clientMediaType = $clientMediaType;
    }
}
