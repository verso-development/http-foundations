<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

use HTTP\Request\Server\ServerRequestFactory;

require '../../vendor/autoload.php';
require '../functions.php';

$serverRequest = ServerRequestFactory::createServerRequestFromGlobals();

debug($serverRequest->getUri());
debug($serverRequest->getQueryParams());
debug($serverRequest->getCookieParams());
